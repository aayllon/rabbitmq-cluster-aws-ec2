terraform {
 backend "s3" {
 encrypt = true
 bucket = "zinio-terraform-state"
 dynamodb_table = "terraform-state-lock-dynamo"
 region = "us-west-2"
  key = "zen-rabbit-asg.tfstate"
 }
}
