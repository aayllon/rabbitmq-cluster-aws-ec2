variable "env" {}
variable "ansible_bucket" {}
variable "sg_name" {}
variable "cidr-sg" {}
variable "zinio-ip" {}
variable "ami" {}
variable "inst_type" {}
variable "vpc-id" {}
variable "root_size" {}
variable "subnets_public" {
   type = "list"
}
variable "subnets_private" {
   type = "list"
}
variable "keyName" {}
variable "target_group_dashboard_arn" {}
variable "target_group_service_arn" {}
variable "asg_max_size" {}
variable "asg_min_size" {}
variable "asg_desired_size" {}
variable "health_period" {}
variable "asg_health_type" {}
variable "lb-name-dash" {}
variable "lb-name-service" {}
