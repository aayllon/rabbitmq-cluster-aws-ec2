resource "aws_iam_role" "ec2-rabbitmq-cluster-role" {
  name = "ec2-rabbitmq-cluster-role-${var.env}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ec2-tag-read-policy" {
  name = "ec2-tag-read-policy-${var.env}"
  role = "${aws_iam_role.ec2-rabbitmq-cluster-role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:DescribeInstances"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "s3-automation-read-access-policy" {
  name = "s3-automation-read-access-policy-${var.env}"
  role = "${aws_iam_role.ec2-rabbitmq-cluster-role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::{var.ansible_bucket}/*"
    },
    {
      "Action": [
      "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::{var.ansible_bucket}/*"
      }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "ec2-profile" {
  name  = "ec2-rabbitmq-cluster-profile-${var.env}"
  roles = ["${aws_iam_role.ec2-rabbitmq-cluster-role.name}" ]
}

resource "aws_security_group" "security-group" {
  name                       = "${var.sg_name}"
  description                = "Rabbit-Sg"
  vpc_id                     = "${var.vpc-id}"

  ingress {
    from_port                = 0
    to_port                  = 0
    protocol                 = "-1"
    cidr_blocks              = [ "${var.cidr-sg}" ]
  }
  ingress {
    from_port                = 15672
    to_port                  = 15672
    protocol                 = "TCP"
    cidr_blocks              = [ "${var.zinio-ip}" ]
  }
  egress {
    from_port                = 0
    to_port                  = 0
    protocol                 = "-1"
    cidr_blocks              = [ "0.0.0.0/0" ]
  }
  tags = {
    Name                     = "Rabbit-cluster-sg-${var.env}"
  }
}

resource "aws_lb" "network-lb-dash" {
  name               = "${var.lb-name-dash}"
  internal           = false
  load_balancer_type = "network"
  subnets            = ["${var.subnets_public}"]

  enable_deletion_protection = false

  tags = {
    Environment = "Rabbit-cluster-${var.env}"
  }
}

resource "aws_lb" "network-lb-service" {
  name               = "${var.lb-name-service}"
  internal           = true
  load_balancer_type = "network"
  subnets            = ["${var.subnets_private}"]

  enable_deletion_protection = false

  tags = {
    Environment = "Rabbit-cluster-${var.env}"
  }
}


resource "aws_lb_listener" "lb_listener-dash" {
  load_balancer_arn = "${aws_lb.network-lb-dash.arn}"
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${var.target_group_dashboard_arn}"
  }
}

resource "aws_lb_listener" "lb_listener-service" {
  load_balancer_arn = "${aws_lb.network-lb-service.arn}"
  port              = "5672"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${var.target_group_service_arn}"
  }
}



resource "aws_launch_configuration" "rabbit-lc" {
  image_id                    = "${var.ami}"
  instance_type               = "${var.inst_type}"
  iam_instance_profile        = "${aws_iam_instance_profile.ec2-profile.name}"
  key_name                    = "${var.keyName}"
  security_groups             = [ "${aws_security_group.security-group.id}" ]
  root_block_device {
    volume_size               = "${var.root_size}"
  }
  user_data                   = <<EOF
    #!/bin/bash
    set -ex
    sudo yum install python-pip -y
    sudo pip install ansible
    sudo aws s3 cp s3://com.zinio.operations.automation/ansible_rabbitmq_cluster/ansible_rabbitmq_cluster-0.0.1.zip /
    sudo unzip /ansible_rabbitmq_cluster-0.0.1.zip
    sudo ansible-playbook -e @/roles/rabbitmq/vars/"${var.env}".yml rabbitmq-playbook.yml
    EOF

  lifecycle {
    create_before_destroy     = true
  }
}


resource "aws_autoscaling_group" "rabbitmq-asg" {
  name                      = "rabbitmq-asg-${var.env}"
  max_size                  = "${var.asg_max_size}"
  min_size                  = "${var.asg_min_size}"
  health_check_grace_period = "${var.health_period}"
  health_check_type         = "${var.asg_health_type}"
  desired_capacity          = "${var.asg_desired_size}"
  force_delete              = false
  launch_configuration      = "${aws_launch_configuration.rabbit-lc.name}"
  vpc_zone_identifier       = ["${var.subnets_private}"]
  target_group_arns         = ["${var.target_group_service_arn}", "${var.target_group_dashboard_arn}"]
  tag {
    key                 = "Name"
    value               = "rabbitmq-cluster-${var.env}"
    propagate_at_launch = true
  }
  tag {
    key                 = "environment"
    value               = "${var.env}"
    propagate_at_launch = true
  }
  tag {
    key                 = "service"
    value               = "rabbitmq-cluster"
    propagate_at_launch = true
  }
}
