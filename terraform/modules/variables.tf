provider "aws" {
  region     = "us-west-2"
  version = "~> 1.7.1"
}

variable "env" {
  description = "In which environment will run this infra"
}

variable "ansible_bucket" {
  description = "S3 bucket name where ansible playbook is stored. This playbook install and configure the rabbit cluster"
}

variable "sg_name" {
  description = "Name of the applied security group"
}

variable "cidr-sg" {
  description = "Cidr block of the local network, used to allow all incoming traffic from this network within the SG"
}

variable "public-ip" {
  description = "Public ip allowed to access Rabbitmq dashboard"
}

variable "ami" {
  description = "Ami id used for the new instance. Aws Linux-2"
}

variable "inst_type" {
  description = "Instance type to create"
}

variable "vpc-id" {
  description = "vpc where infra will run"
}

variable "root_size" {
  description = "Instance's root volume size"
}

variable "subnets_public" {
  description = "Subnets list where the lb will listen, it has to contain a public subnets list"
  type = "list"
}

variable "subnets_private" {
  description = "Subnets list where instances will run"
  type = "list"
}

variable "keyName" {
  description = "Key-pair used to connect to the instances"
}

variable "target_group_dashboard_arn" {
  description = "Target group Arn pointing to instances dashboard port"
}

variable "target_group_service_arn" {
  description = "Target group Arn pointing to instances service port"
}

variable "asg_max_size" {
  description = "Max number of running instances on the ASG"
}

variable "asg_min_size" {
  description = "Min number of running instances on the AG"
}

variable "asg_desired_size" {
  description = "Desired number of running instances on the ASG"
}

variable "health_period" {
  description = "Time to wait until considered instance fully started"
}

variable "asg_health_type" {
  description = "Health-check used by the ASG"
}

variable "lb-name-dash" {
  description = "Name for the dashboard network load balancer"
}

variable "lb-name-service" {
  description = "Name for the service network load balancer"
}
